from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        employee_id = content.get('employee_id')

        if employee_id is None:
            return JsonResponse(
                {"message": "Employee ID is required"},
                status=400,
            )
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            tech_id = content.get("technician")
            technician = Technician.objects.get(id=tech_id)
            content["technician"] = technician
            content["status"] = "Created"
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400,
            )
        
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    
@require_http_methods(["DELETE"])
def api_delete_technician(request, tech_id):

    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=tech_id)
            technician.delete()
            return JsonResponse(
                {"deleted": True},
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
    
@require_http_methods(["DELETE"])
def api_delete_appointment(request, appointment_id):

    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=appointment_id)
            appointment.delete()
            return JsonResponse(
                {"deleted": True},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment"},
                status=400,
            )
        
@require_http_methods(["PUT"])
def api_cancel_appointment(request, appointment_id):

    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=appointment_id)
            appointment.status = "Canceled"
            appointment.save()
            return JsonResponse(
                    {"message": "Canceled"},
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                    {"message": "Failed"},
                )

@require_http_methods(["PUT"])
def api_finish_appointment(request, appointment_id):

    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=appointment_id)
            appointment.status = "Finished"
            appointment.save()
            return JsonResponse(
                    {"message": "Finished"},
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                    {"message": "Failed"},
                )

        
            
    


