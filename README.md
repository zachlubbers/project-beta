# CarCar

This application can be used to manage sales, inventory, and service departments of a car dealership.

Team:

* Zach - Sales
* Cian - Service

## How to run this app
1. Fork this repository: https://gitlab.com/zachlubbers/project-beta
2. Clone using: git clone 'repository.url
3. Build and run the project via docker by typing the following commands in your terminal while in the main project directory:
    docker volume create beta-data
    docker-compose build
    docker-compose up
4. Once all containers are running on Docker the application can be viewed in the browser at http://localhost:3000/

## Design
The CarCar application is comprised of 3 microservices: sales, service, and inventory. The sales and service microservices pull cars from the inventory
to interact with.

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

The sales microservice has 4 classes:
    -Salesperson
    -Customer
    -AutomobileVO is a value object that is polled from the inventory microservice model Automobile
    -Sale which implements all 3 of the above models as foreign keys

## Project diagram

!img[Img](/images/CarCar_DDD_diagram.PNG)

## CRUD documentation

## Manufacturers
GET List maufacturers - http://localhost:8100/api/manufacturers/
    -Returns:
    {
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chevy"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	]
}

POST Create a manufacturer - http://localhost:8100/api/manufacturers/
    -Send:
    {
	"name": "Ford"
}

    -Returns:
    {
	"href": "/api/manufacturers/3/",
	"id": 3,
	"name": "Ford"
}
GET View a specific manufacturer http://localhost:8100/api/manufacturers/id
    Returns:
    {
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Toyota"
}
DELETE Delete a manufacturer http://localhost:8100/api/manufacturers/id
    Returns:
    {
	"id": null,
	"name": "Ford"
}

## Vehicle Models
GET List vehicle models - http://localhost:8100/api/models/
    Returns:
    {
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Impala",
			"picture_url": "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.com%2Fpin%2Fchevrolet-impala-impala-chevy-impala--169236898474981154%2F&psig=AOvVaw2_74eZGO8G_FHIAcgO-0i8&ust=1707348920249000&source=im",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chevy"
			}
		},
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "Tacoma",
			"picture_url": "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.toyotaknoxville.com%2Fblog-toyota-tacoma-model-timeline.html&psig=AOvVaw0XrlAJHPvQaSWPsqTj6q9P&ust=1707268788711000&source=images&cd=vfe&opi=89978",
			"manufacturer": {
				"href": "/api/manufacturers/2/",
				"id": 2,
				"name": "Toyota"
			}
		}
	]
}
POST Create a vehicle model - http://localhost:8100/api/models/
    -Send:
    {
	"name": "F350",
	"picture_url": "imageurl.com",
	"manufacturer_id": 4
}
    -Returns:
    {
	"href": "/api/models/3/",
	"id": 3,
	"name": "F350",
	"picture_url": "imageurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "Ford"
	}
}
GET View a specific vehicle model - http://localhost:8100/api/models/id
    -Returns:
    {
	"href": "/api/models/3/",
	"id": 3,
	"name": "F350",
	"picture_url": "imageurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "Ford"
	}
}
DELETE Delete a vehicle model - http://localhost:8100/api/models/id
    Returns:
    {
	"id": null,
	"name": "F350",
	"picture_url": "imageurl.com",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "Ford"
	}
}
## Automobiles
GET List of automobiles - http://localhost:8100/api/automobiles/
    Returns:
    "autos": [
		{
			"href": "/api/automobiles/fjfjfjfjfjfjfjfjf/",
			"id": 1,
			"color": "purple",
			"year": 1968,
			"vin": "fjfjfjfjfjfjfjfjf",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Impala",
				"picture_url": "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.com%2Fpin%2Fchevrolet-impala-impala-chevy-impala--169236898474981154%2F&psig=AOvVaw2_74eZGO8G_FHIAcgO-0i8&ust=1707348920249000&source=im",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chevy"
				}
			},
			"sold": false
		},
	]

POST Create new automobile - http://localhost:8100/api/automobiles/
    -Send:
{
	"color": "yellow",
	"year": 1978,
	"vin": "numbersandletters",
	"model_id": 3
}
    -Returns:
{
	"href": "/api/automobiles/numbersandletters/",
	"id": 3,
	"color": "yellow",
	"year": 1978,
	"vin": "numbersandletters",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "F350",
		"picture_url": "imageurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Ford"
		}
	},
	"sold": false
}
GET Specific automobile -http://localhost:8100//api/automobiles/vin
    Returns:
{
	"href": "/api/automobiles/fjfjfjfjfjfjfjfjf/",
	"id": 1,
	"color": "purple",
	"year": 1968,
	"vin": "fjfjfjfjfjfjfjfjf",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Impala",
		"picture_url": "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.com%2Fpin%2Fchevrolet-impala-impala-chevy-impala--169236898474981154%2F&psig=AOvVaw2_74eZGO8G_FHIAcgO-0i8&ust=1707348920249000&source=im",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chevy"
		}
	},
	"sold": false
}
DELETE Delete automobile - http://localhost:8100//api/automobiles/vin
    Returns:
{
	"href": "/api/automobiles/numbersandletters/",
	"id": null,
	"color": "yellow",
	"year": 1978,
	"vin": "numbersandletters",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "F350",
		"picture_url": "imageurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Ford"
		}
	},
	"sold": false
}

## Salespeople
GET List salespeople - http://localhost:8090/api/salespeople/
    Returns:
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "John",
			"last_name": "Seller",
			"employee_id": "JSeller"
		},
		{
			"id": 2,
			"first_name": "Frank",
			"last_name": "Thetank",
			"employee_id": "fff"
		}
	]
}
POST Create salesperson - http://localhost:8090/api/salespeople/http://localhost:8090/api/salespeople/
    Send:
{
	"first_name": "John",
	"last_name": "Seller",
	"employee_id": "JSeller"
}
    Returns:
{
	"id": 1,
	"first_name": "John",
	"last_name": "Seller",
	"employee_id": "JSeller"
}
GET Show specific salesperson - http://localhost:8090/api/salespeople/id
    Returns:
{
	"id": 1,
	"first_name": "John",
	"last_name": "Seller",
	"employee_id": "JSeller"
}
DELETE Delete salesperson - http://localhost:8090/api/customers/2
    Returns:
{
	"deleted": true
}

## Customers
GET List customers - http://localhost:8090/api/customers/
    Returns:
{
	"customers": [
		{
			"id": 1,
			"first_name": "Jimmy",
			"last_name": "Carbuyer",
			"address": "123 Road St",
			"phone_number": 123485033
		},
		{
			"id": 2,
			"first_name": "f",
			"last_name": "ff",
			"address": "123 f st",
			"phone_number": 121212121
		}
	]
}
POST Create customer - http://localhost:8090/api/customers/
    -Send:
{
	"first_name": "Jimmy",
	"last_name": "Carbuyer",
	"address": "123 Road St",
	"phone_number": "123485033"
}
    -Returns:
{
	"id": 1,
	"first_name": "Jimmy",
	"last_name": "Carbuyer",
	"address": "123 Road St",
	"phone_number": "123485033"
}
GET Show specific customer - http://localhost:8090/api/customers/id
    Returns:
{
	"id": 1,
	"first_name": "Jimmy",
	"last_name": "Carbuyer",
	"address": "123 Road St",
	"phone_number": 123485033
}
DELETE Delete a customer - http://localhost:8090/api/customers/id
    Returns:
{
	"deleted": true
}

## Sales
GET Show list of sales - http://localhost:8090/api/sales
    Returns:
{
	"sales": [
		{
			"id": 1,
			"price": 2700,
			"automobile": {
				"id": 1,
				"vin": "fjfjfjfjfjfjfjfjf",
				"sold": false
			},
			"salesperson": {
				"id": 1,
				"first_name": "John",
				"last_name": "Seller",
				"employee_id": "JSeller"
			},
			"customer": {
				"id": 1,
				"first_name": "Jimmy",
				"last_name": "Carbuyer",
				"address": "123 Road St",
				"phone_number": 123485033
			}
		}
	]
}
POST Create a sale - http://localhost:8090/api/sales
    -Send:
{

	"price": 2700,
	"automobile_id": 1,
	"salesperson_id": 1,
	"customer_id": 1

}
    -Returns:
{
	"id": 2,
	"price": 2700,
	"automobile": {
		"id": 1,
		"vin": "fjfjfjfjfjfjfjfjf",
		"sold": false
	},
	"salesperson": {
		"id": 1,
		"first_name": "John",
		"last_name": "Seller",
		"employee_id": "JSeller"
	},
	"customer": {
		"id": 1,
		"first_name": "Jimmy",
		"last_name": "Carbuyer",
		"address": "123 Road St",
		"phone_number": 123485033
	}
}
GET Show specific sale - http://localhost:8090/api/sales/id
    Returns:
{
	"id": 1,
	"price": 2700,
	"automobile": {
		"id": 1,
		"vin": "fjfjfjfjfjfjfjfjf",
		"sold": false
	},
	"salesperson": {
		"id": 1,
		"first_name": "John",
		"last_name": "Seller",
		"employee_id": "JSeller"
	},
	"customer": {
		"id": 1,
		"first_name": "Jimmy",
		"last_name": "Carbuyer",
		"address": "123 Road St",
		"phone_number": 123485033
	}
}
DELETE Delete a sale -http://localhost:8090/api/sales/id
    Returns:
{
	"deleted": true
}

## Technicians

GET Show Techincians - http://localhost:8080/api/technicians/
    {
	"technicians": [
		{
			"id": 1,
			"first_name": "Sparta",
			"last_name": "Ongod",
			"employee_id": "yaeeah"
		},
      ]
    }

POST Techincian - http://localhost:8080/api/technicians/
    Send
    {
	"first_name":"",
	"last_name":"",
	"employee_id":""
}

DELETE techincian - http://localhost:8080/api/technicians/:id/
    When correct Id is sent
    {
	"deleted": true
}

## Appointment

GET appointments - http://localhost:8080/api/appointments/
    Returns
    {
	"appointments": [
		{
			"id": 6,
			"date_time": "2012-09-04T19:00:00+00:00",
			"reason": "wasd",
			"status": "created",
			"vin": "dsdsweasd",
			"customer": "God",
			"technician": {
				"id": 1,
				"first_name": "Sparta",
				"last_name": "Ongod",
				"employee_id": "yaeeah"
			}
		},
	]
}

POST appointments - http://localhost:8080/api/appointments/
    Send Format
    {
	"date_time": "2021-09-04 21:02",
	"reason" : "",
	"status" : "",
	"vin": "",
	"customer": "",
	"technician":""
}

DELETE appointment - http://localhost:8080/api/appointments/:id
    When correct Id is sent
    {
	"deleted": true
}
