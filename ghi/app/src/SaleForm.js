import React, {useState, useEffect} from 'react'

function SaleForm () {
    const [automobiles, setVin] = useState([])
    const [salespeople, setSalesperson] = useState([])
    const [customers, setCustomer] = useState([])
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: ''
    })
    const getVins = async () => {
        const vinUrl = 'http://localhost:8100/api/automobiles/'
        const vinResponse = await fetch(vinUrl)
        if (vinResponse.ok) {
            const vinData = await vinResponse.json()
            const unsoldCars = vinData.autos.filter(auto => !auto.sold)
            setVin(unsoldCars)
        }
    }
    const getSalespeople = async () => {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const salespersonResponse = await fetch(salespersonUrl)
        if (salespersonResponse.ok) {
            const salespersonData = await salespersonResponse.json()
            setSalesperson(salespersonData.salespeople)
        }
    }
    const getCustomers = async () => {
        const customerUrl = 'http://localhost:8090/api/customers'
        const customerResponse = await fetch(customerUrl)
        if (customerResponse.ok) {
            const customerData = await customerResponse.json()
            setCustomer(customerData.customers)
        }
    }
useEffect(() => {
    getVins()
    getSalespeople()
    getCustomers()
}, [])
console.log("form data:", formData)
const handleSubmit = async (event) => {
    event.preventDefault()
    const saleUrl = 'http://localhost:8090/api/sales/'

    const fetchConfig = {
        method: "post",
        body: JSON.stringify({
            vin: formData.automobile.vin,
            salesperson_id: formData.salesperson,
            customer_id: formData.customer,
            price: formData.price
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    try {
        const response = await fetch(saleUrl, fetchConfig)
        if (response.ok) {
            console.log('Sale created successfully:', response)
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: ''
            })
        } else {
            console.error('Error creating sale:', response)
        }
    } catch (error) {
        console.error('Error during fetch:', error)
    }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name




        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="form-floating mb-3">
                            <select value={formData.automobile} onChange={handleFormChange} name="automobile" id="automobile" className="formSelect">
                                <option value="">Choose a VIN</option>
                                {automobiles.map(auto => {
                                    return(
                                        <option key={auto.vin} value={auto.vin}>
                                            {auto.vin}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select value={formData.salesperson} onChange={handleFormChange} name="salesperson" id="salesperson" className="formSelect">
                                <option value="">Choose a Salesperson</option>
                                {salespeople.map(salesperson => {
                                    return(
                                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                            {salesperson.employee_id}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select value={formData.customer} onChange={handleFormChange} name="customer" id="customer" className="formSelect">
                                <option value="">Choose a Customer</option>
                                {customers.map(customer => {
                                    return(
                                        <option key={`${customer.first_name}_${customer.last_name}`} value={`${customer.first_name}_${customer.last_name}`}>
                                            {`${customer.first_name} ${customer.last_name}`}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.price} onChange={handleFormChange} placeholder="price" required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Add sale</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SaleForm
