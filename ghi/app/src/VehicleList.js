import React, {useState, useEffect } from 'react';

function ListVehicles(){
    const [autos, setAutos] = useState([]);

    const getData = async () => {
        const responce = await fetch("http://localhost:8100/api/models/");

        if (responce.ok) {
            const data = await responce.json();
            setAutos(data.models)
        }
    }

    useEffect(()=>{
        getData();
    }, [])

    return(

        <>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {autos.map(auto => {
              return (
                <tr key={auto.id}>
                  <td>{ auto.name }</td>
                  <td>{ auto.manufacturer.name }</td>
                  <td><img src={auto.picture_url} className="img-thumbnail" alt="My PICTURE"></img></td>
                </tr>

              );
            })}
          </tbody>
        </table>
        </>
      );


}

export default ListVehicles;
