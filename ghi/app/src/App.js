import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListTechs from './ListTechs';
import CreateTechForm from './CreateTechForm';
import ServiceHistory from './ServiceHistory';
import ServiceAppointments from './ServiceAppointments';
import CreateAppointment from './CreateAppointment';
import ListSalespeople from './ListSalespeople'
import SalespersonForm from './SalespersonForm'
import CustomerList from './ListCustomer'
import CustomerForm from './CustomerForm'
import SalesList from './ListSales'
import SalesForm from './SaleForm'
import SalespersonHistory from './SalespersonHistory'
import ListVehicles from './VehicleList';
import VehicleForm from './VehicleForm';
import ManufacturerList from './ListManufacturers'
import ManufacturerForm from './ManufacturerForm'
import AutomobileList from './ListAutomobiles'
import AutomobileForm from './AutomobileForm'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/" element={<ListTechs />} />
          <Route path="/technicians/new" element={<CreateTechForm />} />
          <Route path="/appointmentshistory/" element={<ServiceHistory />} />
          <Route path="/appointments/" element={<ServiceAppointments />} />
          <Route path="/appointments/new" element={<CreateAppointment />} />
          <Route path="/salespeople" element={<ListSalespeople />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/salespeople/history" element={<SalespersonHistory />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/new" element={<SalesForm />} />
          <Route path="/vehicles" element={<ListVehicles />} />
          <Route path="/vehicles/new" element={<VehicleForm />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
