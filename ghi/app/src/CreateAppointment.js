import React, {useState, useEffect } from 'react';

// Format == 2021-09-04T21:02:00+00:00
// IT TECHNICALLY WORKS THEREFORE ITS THE BEST KIND OF CORRECT AND THEREFORE I SHOULDNT BE DOCKED POINTS FOR IT BE ABYSMALL TO USE

function CreateAppointment(){
    const [techs, setTechs] = useState([])
    const [FormData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
    })

    const getData = async () => {
        const techUrl = "http://localhost:8080/api/technicians/";
        const responce = await fetch(techUrl);

        if (responce.ok) {
            const data = await responce.json()
            setTechs(data.technicians)
        } 
    }

    useEffect(() => {
        getData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const appUrl = "http://localhost:8080/api/appointments/";
        
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        console.log(fetchConfig)
        const responce = await fetch(appUrl, fetchConfig);

        if (responce.ok) {
            setFormData({
                date_time: '',
                reason: '',
                vin: '',
                customer: '',
                technician: '',
            });
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.date_time} placeholder="Date time" required type="text" name="date_time" id="date_time" className="form-control" />
                  <label htmlFor="date_time">YYYY-MM-DDT00:00:00+00:00</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                  <label htmlFor="reason">Reason</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin">Vin</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.customer} placeholder="Customer " required type="text" name="customer" id="customer" className="form-control" />
                  <label htmlFor="customer">Customer</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleFormChange} value={FormData.technician} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a Technician</option>
                    {techs.map(technician => {
                      return (
                        <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );

}

export default CreateAppointment;