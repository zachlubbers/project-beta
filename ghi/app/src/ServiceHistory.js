import React, {useState, useEffect } from 'react';

function ServiceHistory(){
    const [apps, setApps] = useState([])
    const [SelectedVin, setVin] = useState([])
    console.log(SelectedVin)

    const setvin = (e) => {
        setVin(e.target.value)
    }

    const getData = async () => {
        const techUrl = "http://localhost:8080/api/appointments/";
        const responce = await fetch(techUrl);

        if (responce.ok) {
            const data = await responce.json()
            setApps(data.appointments)
        } 
    }

    useEffect(() => {
        getData();
    }, []);

    const searchAppointments = apps.filter(appointment =>
        appointment.vin.includes(SelectedVin)
        );

    return (
        
        <>
          <div className="search">
            <h1>Service History</h1>      
            <div className="form-floating mb-3">
            <input onChange={setvin} value={SelectedVin} placeholder="Vin" required type="text" name="vin" id="vin" className="Vin-search" />
                    <button className="btn btn-primary">Search By Vin</button>
                </div>
          </div>
          <div>
          <table className="table">
          <thead>
            <tr>
              <th>Date Time</th>
              <th>Reason</th>
              <th>Status</th>
              <th>Vin</th>
              <th>Customer</th>
              <th>Technician</th>
            </tr>
          </thead>
          <tbody>
            {searchAppointments.map((app) => { 
              return (
                <tr key={app.id}>
                  <td>{ app.date_time }</td>
                  <td>{ app.reason }</td>
                  <td>{ app.status }</td>
                  <td>{ app.vin }</td>
                  <td>{ app.customer }</td>
                  <td>{ app.technician.first_name }</td>
                </tr>

              );
            })}
          </tbody>
          </table>
          </div>
        </>
      );
    
}


export default ServiceHistory;