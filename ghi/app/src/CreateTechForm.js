import React, {useState, useEffect } from 'react';

function CreateTechForm(){
    const [FormData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const techUrl = "http://localhost:8080/api/technicians/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const responce = await fetch(techUrl, fetchConfig);

        if (responce.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        }

    }
    

    return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Join The Team!</h1>
          <form onSubmit={handleSubmit} id="create-tech-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.employee_id} placeholder="ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
              <label htmlFor="employee_id">ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}


export default CreateTechForm;