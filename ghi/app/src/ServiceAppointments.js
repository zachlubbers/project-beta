import React, {useState, useEffect } from 'react';

function ServiceAppointments(){
    const [apps, setApps] = useState([])

    const getData = async () => {
        const responce = await fetch ("http://localhost:8080/api/appointments/");
        
        if (responce.ok) {
            const data = await responce.json();
            setApps(data.appointments)
            console.log(data.appointments)
        }
    }
    
    useEffect(()=>{
        getData();
    }, [])

    const CancelAppointment = async (event, id) => {
      event.preventDefault();
      const appUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
  
      const fetchConfig = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
      };
      await fetch(appUrl, fetchConfig)
      // console.log(appUrl)
      // console.log(fetchConfig)
      const responce = await fetch(appUrl, fetchConfig);
      if (responce.ok) {
        getData();
      }

    }

    const FinishAppointment = async (event, id) => {
      event.preventDefault();
      const appUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
  
      const fetchConfig = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
      };
      await fetch(appUrl, fetchConfig)
      const responce = await fetch(appUrl, fetchConfig);
      if (responce.ok) {
        getData();
      }

    }


    return(
        
        <>
        <table className = "table">
          <thead>
            <tr>
              <th>Date Time</th>
              <th>Reason</th>
              <th>Status</th>
              <th>Vin</th>
              <th>Customer</th>
              <th>Technician</th>
            </tr>
          </thead>
          <tbody>
            {apps.map(app => {if (app.status === "Canceled" || app.status === "Finished") {return null}
              return (
                <tr key={app.id}>
                  <td>{ app.date_time }</td>
                  <td>{ app.reason }</td>
                  <td>{ app.status }</td>
                  <td>{ app.vin }</td>
                  <td>{ app.customer }</td>
                  <td>{ app.technician.first_name }</td>
                  <td><button onClick= {(event) => CancelAppointment(event, app.id)} className="btn btn-primary">Cancel</button></td>
                  <td><button onClick= {(event) => FinishAppointment(event, app.id)} className="btn btn-primary">Finish</button></td>
                  </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );

}

export default ServiceAppointments;