import React, {useState, useEffect } from 'react';

function VehicleForm(){
    const [manu, setManufacturers] = useState([])
    const [FormData, setFormData] = useState({
        name: '',
        manufacturer_id: '',
        picture_url: '',

    })

    const getData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        getData();
    }, []);


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const VehicleUrl = "http://localhost:8100/api/models/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        console.log(fetchConfig)
        const responce = await fetch(VehicleUrl, fetchConfig);

        if (responce.ok) {
            setFormData({
                name: '',
                manufacturer_id: '',
                picture_url: '',
        
            });
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Make a Vehicle</h1>
              <form onSubmit={handleSubmit} id="create-vehicle-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <select onChange={handleFormChange} value={FormData.manufacturer_id} placeholder="manufacturer_id" required name="manufacturer_id" id="manufacturer_id" className="form-select">
                    <option value="">Choose a Manufacturer</option>
                    {manu.map(manufacturer => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.picture_url} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                  <label htmlFor="Picture">Picture Url</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    

}

export default VehicleForm;