import { useEffect, useState } from 'react'

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([])
    const [sales, setSales] = useState([])
    const [selectedSalesperson, setSelectedSalesperson] = useState({})

    const getSalesperson = async () => {
        const salespersonResponse = await fetch('http://localhost:8090/api/salespeople/')
        if (salespersonResponse.ok) {
            const salespersonData = await salespersonResponse.json()
            setSalespeople(salespersonData.salespeople)
        }
    }

    const getSales = async () => {
        const salesResponse = await fetch('http://localhost:8090/api/sales/')
        if (salesResponse.ok) {
            const salesData = await salesResponse.json()
            setSales(salesData.sales)
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            await getSalesperson()
            await getSales()
        }
        fetchData()
    }, [])

    const handleSalespersonChange = (e) => {
        const selectedSalespersonId = e.target.value
        const selectedSalespersonObject = salespeople.find(salesperson => salesperson.id === parseInt(selectedSalespersonId, 10))
        setSelectedSalesperson(selectedSalespersonObject)
    }

    return (
        <>
            <h1>Salesperson History</h1>
            <div>
                <select value={selectedSalesperson.id} onChange={handleSalespersonChange} name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeople.map(salesperson => (
                        <option key={salesperson.id} value={salesperson.id}>{`${salesperson.first_name} ${salesperson.last_name}`}</option>
                    ))}
                </select>
            </div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Object.keys(selectedSalesperson).length !== 0 && sales.filter(sale => sale.salesperson.id === selectedSalesperson.id).map(sale => (
                            <tr key={sale.id}>
                                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default SalespersonHistory
