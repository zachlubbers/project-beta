import React, {useState, useEffect } from 'react';

function ListTechs(){
    const [techs, setTechs] = useState([]);

    const yeetData = async () => {
        const responce = await fetch("http://localhost:8080/api/technicians/");

        if (responce.ok) {
            const data = await responce.json();
            setTechs(data.technicians)
        }
    }


    useEffect(()=>{
        yeetData();
    }, [])

    return(
        
        <>
        <table className='table'> 
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Employee Id</th>
            </tr>
          </thead>
          <tbody>
            {techs.map(tech => {
              return (
                <tr key={tech.id}>
                  <td>{ tech.first_name }</td>
                  <td>{ tech.last_name }</td>
                  <td>{ tech.employee_id }</td>
                </tr>

              );
            })}
          </tbody>
        </table>
        </>
      );

    
}

export default ListTechs;