import { useEffect, useState } from 'react';

function AutomobileList() {
    const [autos, setAutomobiles] = useState([]);

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                setAutomobiles(data.autos);
            } else {
                console.error('Failed to fetch data:', response.status);
            }
        } catch (error) {
            console.error('Error during fetch:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {autos.map((automobile) => (
                    <tr key={automobile.href}>
                        <td>{automobile.vin}</td>
                        <td>{automobile.color}</td>
                        <td>{automobile.year}</td>
                        <td>{automobile.model.name}</td>
                        <td>{automobile.model.manufacturer.name}</td>
                        <td>{automobile.sold ? 'Yes' : 'No'}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default AutomobileList;
