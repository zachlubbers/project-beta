import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
from sales_rest.models import AutomobileVO
# from sales_rest.models import Something


def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    print(response)
    content = json.loads(response.content)
    print(content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin = auto["vin"],
            sold = auto["sold"]
        )


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()
            # Do not copy entire file


        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
